import React, { Component } from "react";


class SearchBar extends Component{

    state = {term: ""}

    handleOnClick = (event) => {
        //console.log(event.target.value);
    }

    onFormSubmit = (event) => {
        event.preventDefault();
        //console.log(this.state.term);
        ///data send parent to child 
        console.log(this.props.name);
        
        ///data send child to parent using function
        this.props.SearchSubmit(this.state.term);
        
        
    }

    render(){
        return(
            <div className="ui segment">
                <form onSubmit={(event) => this.onFormSubmit(event)} className="ui form">
                    <div className="field">
                         <label>Image Search</label>
                         <input 
                            type="text"
                            value={this.state.term}
                            onClick = {this.handleOnClick}
                            onChange={(event) => { this.setState({term : event.target.value}) }}

                         />   
                    </div>
                </form>
                Term:{this.state.term}
            </div>
        )
    }
}

export default SearchBar;