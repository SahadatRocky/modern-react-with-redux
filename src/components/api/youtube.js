
import axios from "axios"

const KEY = "AIzaSyDM0eOVcVBUYtk5vjTSkRO38fDVXNMQIas";
export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3',
    params:{
        part:'snippet',
        maxResults: 5,
        key: KEY
    }
});