
//create action
const selectSong = (song) => {
    //action
    return(
      {
        type: "SELECTED_SONG",
        payload: song 
      }
    )
}

export default selectSong;