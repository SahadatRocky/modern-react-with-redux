import axios from 'axios';
import React, {useState, useEffect} from 'react';

const Search = () => {

    const [term, setTerm] =  useState('programming');
    const [results, setResults] = useState([]);
    const [debouncedTerm, setDebouncedTerm] = useState(term);
    console.log("i run with every Render");

    useEffect(() => {
        const setTimeoutId = setTimeout(() => {
             setDebouncedTerm(term); 
        },1000);

        return() => {
            clearTimeout(setTimeoutId);
        } 
    },[term]);


    useEffect( () => {
       // console.log("i run after every Render"+ term);
       const search =async () => { 
        const response =  await axios.get('https://en.wikipedia.org/w/api.php', {
               params:{
                   action:'query',
                   list: 'search',
                   origin: '*',
                   format: 'json',
                   srsearch: debouncedTerm
               }            
       });
       //console.log('---->>>>',data);
       setResults(response.data.query.search);

      };

    //   if(term && !results.length){
    //     search();
    //   }else{
    //     const timeoutid = setTimeout(() => {
    //         if(term){
    //             search();
    //           }
    //       },500);
    
    //       return() => {
    //         clearTimeout(timeoutid);
    //       };
    //   }
    search();
    },[debouncedTerm, results.length]);
    


    const renderedResults = results.map((result) => {
          return(
              <div key={result.pageid} className="item">
                  
                      <div className="right floated content">
                         <a 
                            className="ui button"
                            href={`https://en.wikipedia.org?curid=${result.pageid}`}>
                            GO
                        </a>
                      </div>
                      <div className="content">
                      <div className="header">
                            {result.title}
                      </div>
                            {result.snippet}
                  </div>
              </div>
          );
    });

    return(
        <div>
            <div className="ui form">
                <div className="field">
                      <label>Search</label>
                      <input 
                        type="text"
                        value={term}
                        onChange ={(e) => setTerm(e.target.value)}
                        className="input"
                        />
                </div>
            </div>
            <div className="ui called list">
                {renderedResults}
            </div>
        </div>
    );
};

export default Search;