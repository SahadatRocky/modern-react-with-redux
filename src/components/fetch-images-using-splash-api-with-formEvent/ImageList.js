import React from "react";
import ImageCard from '../fetch-images-using-splash-api-with-formEvent/ImageCard'
const ImageList = (props) =>{

   return props.images.map( (image) => {
           return(<div className="image-list">
               <ImageCard key={image.id} image={image}/>
           </div>) 
    });
    // return(
    //     <div className="image-list">
            
    //     </div>
    // );
}

export default ImageList;