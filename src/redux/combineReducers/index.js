
import { combineReducers } from "redux"
import postsReducer from "../redux-blog/reducers/postsReducer";
import usersReducer from "../redux-blog/reducers/usersReducer";
import songReducers from "../redux-song/reducers/songReducers";
import selectedSongReducers from "../redux-song/reducers/selectedSongReducers";
import {reducer as formReducer} from 'redux-form'; 
import AuthReducer from "../../components/stream-video-client/reducer/AuthReducer";
import streamReducer from "../../components/stream-video-client/reducer/streamReducer";
export default combineReducers({
    songs: songReducers,
    selectedSong: selectedSongReducers,
    posts: postsReducer,
    users: usersReducer,
    auth: AuthReducer,
    form: formReducer,
    streams: streamReducer

})