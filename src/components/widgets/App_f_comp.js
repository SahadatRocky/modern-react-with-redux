
// import React, {useState} from 'react';
// import Accordion from './Accordion';
// import Search from './Search';
// import Dropdown from './Dropdown';
// import Translate from './Translate';
// import Route from '../router/Route';
// import Header from './Header';
// const options = [
//     {
//         label: 'The color red',
//         value: 'red'
//     },
//     {
//         label: 'The color green',
//         value: 'green'
//     },
//     {
//         label: 'The color blue',
//         value: 'blue'
//     }
// ];

// const items = [
//     {
//         title:"What is React?",
//         content: "React is a front-end javascript framework"
//     },
//     {
//         title:"why use React?",
//         content: "React is a favorite js library among Engineers"
//     },    
//     {
//         title:"How do you use React?",
//         content: "You use React by creating components"
//     }
// ];



// const App = () => {

//     const [selected,setSelected]= useState(options[0]);

//     return(
//         <div>
//              <Header />
//             <Route path="/">
//                 <Accordion items={items} />
//             </Route> 
              
//             <Route path="/search">
//                 <Search />
//             </Route> 

//             <Route path="/dropdown">
//                 <Dropdown 
//                     selected={selected}
//                     onSelectedChange = {setSelected} 
//                     options={options} />
//             </Route> 

//             <Route path="/translate">
//                 <Translate />
//             </Route> 

//         </div>
//     );
// };

// export default App;