import React, { Component } from 'react';
import {connect} from 'react-redux';
import {fetchStream, editStream} from '../../actions';
import StreamForm from './StreamForm';
import _ from 'lodash';

class StreamEdit extends Component {

    componentDidMount(){
        this.props.fetchStream(this.props.match.params.id);
       // console.log()
    }

    onSubmit = (fromValues) => {
        console.log('streamedit:', fromValues);
       this.props.editStream(this.props.match.params.id,fromValues);
        
    }

    render(){
    if(!this.props.stream){
       return <div>Loading...</div>
    }
    return(
        <div>
            <h3>Edit a stream</h3>
            <StreamForm
            initialValues = {_.pick(this.props.stream, 'title', 'description') }  
            onSubmit={this.onSubmit}/>
        </div>
        // <div>{this.props.stream.title}</div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
    console.log(ownProps);
      return {stream: state.streams[ownProps.match.params.id]}
}

export default connect(mapStateToProps,{fetchStream,editStream})(StreamEdit);