import React,{ Component } from "react";
import {connect} from 'react-redux';
// import {fetchUserbyId} from '../../redux/redux-blog/actions' 
class UserHeader extends Component{
// componentDidMount(){
    //     this.props.fetchUserbyId(this.props.userId);
    // }


    render(){
        const {user} = this.props;
        if(!user){
            return null;
        }
        return(
            <div>
               {user.name}
            </div>
        );
    }
}

const mapStateToProps = (state,ownProps) => {
    console.log(state.users)
    return {user: state.users.find(user => user.id === ownProps.userId)}
}


export default connect(mapStateToProps)(UserHeader);