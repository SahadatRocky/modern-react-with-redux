import React, { Component } from "react";
class ImageCard extends Component {

    // eslint-disable-next-line no-useless-constructor
    constructor(props){
        super(props);
        this.state = {spans: 0};
        this.imageRef = React.createRef(); // 1
    }

    componentDidMount(){
        this.imageRef.current.addEventListener('load', this.setSpans); //3
    }

    setSpans = () => { // 4
        console.log(this.imageRef.current.clientHeight);
        const height = this.imageRef.current.clientHeight;

        const spans = Math.ceil(height/150);

        this.setState({spans: spans});
        
    }

    render(){

        const {description,urls} = this.props.image;

        return(
            <div style={{gridRowEnd: `span ${this.state.spans}`}}>
                <img 
                    ref={this.imageRef} //2
                    alt = {description}
                    src ={urls.regular}
                />
            </div>
        )
    }
     
}

export default ImageCard;

