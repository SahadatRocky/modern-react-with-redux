import React, { Component } from "react";
import Spinner from "../components/spinner";
import SeasonDisplay from "./SeasonDisplay";
import CommentDetails from "./ApprovalCardAndCoomentDetails/CommentDetails";
import faker from "faker";
import ApprovalCard from "./ApprovalCardAndCoomentDetails/ApprovalCard";
import SearchBar from "../components/fetch-images-using-splash-api-with-formEvent/SearchBar";
import unsplash from "./api/unsplash";
import ImageList from "../components/fetch-images-using-splash-api-with-formEvent/ImageList";
import SongList from "../components/songs/SongList";
import SongDetails from "./songs/SongDetails";
import PostList from "../components/blog/PostList";
import youtube from "../components/api/youtube";
import VideoList from "./videos/VideoList";
import VideoDetails from "./videos/VideoDetails";
import SearchBar2 from '../components/videos/SearchBar2';
import LanguageContext from '../components/translate-context/context/LanguageContext';
import ColorContext from '../components/translate-context/context/ColorContext';
import UserCreate from '../components/translate-context/UserCreate';
import { BrowserRouter, Route, Link} from 'react-router-dom';
import StreamCreate from '../components/stream-video-client/streams/StreamCreate';
import StreamDelete from '../components/stream-video-client/streams/StreamDelete';
import StreamEdit from '../components/stream-video-client/streams/StreamEdit';
import StreamList from '../components/stream-video-client/streams/StreamList';
import StreamShow from '../components/stream-video-client/streams/StreamShow';
import Header from './stream-video-client/Header';


class App extends Component {
  state = { lit: null, errorMessage: "",language: 'english', images: [], videos: [], onVideoSelect: null};
  // eslint-disable-next-line no-useless-constructor
  constructor(props) {
    super(props);
    //this.state = {lit:null, errorMessage:'error found'}
  }

  componentDidMount() {
    window.navigator.geolocation.getCurrentPosition(
      (position) => {
        this.setState({ lit: position.coords.latitude });
      },
      (err) => {
        this.setState({ errorMessage: err.message });
      }
    );

    this.onTermSubmit("bmw");
  }

  renderContent() {
    if (this.state.errorMessage && !this.state.lit) {
      return <div>{this.state.errorMessage}</div>;
    }
    if (!this.state.errorMessage && this.state.lit) {
      return (
        <div>
          <SeasonDisplay lit={this.state.lit} />
        </div>
      );
    }
    return <Spinner message="please accept location request" />;
  }

  apporovalObj = [
    {
      id: 1,
      autor: "Rocky",
      timeToGo: "Today at 5:42PM",
      content: "This has been very useful for my research. Thanks as well!",
      avatar: faker.image.avatar(),
    },
    {
      id: 2,
      autor: "Rossi",
      timeToGo: "Today at 2:42PM",
      content: "that's Awesome. Thanks as well!",
      avatar: faker.image.avatar(),
    },
    {
      id: 3,
      autor: "Azam",
      timeToGo: "Today at 1:42PM",
      content: "that's Awesome. Thanks as well!",
      avatar: faker.image.avatar(),
    },
  ];

  RenderListOfApproval = () => {
    return this.apporovalObj.map((obj) => {
      return (
        <div key={obj.id}>
          <ApprovalCard>
            <CommentDetails
              autor={obj.autor}
              timeToGo={obj.timeToGo}
              content={obj.content}
              avatar={obj.avatar}
            />
          </ApprovalCard>
        </div>
      );
    });

    /*    <ApprovalCard>
            <CommentDetails 
              autor="Rocky"
              timeToGo="Today at 5:42PM" 
              content="This has been very useful for my research. Thanks as well!"
              avatar={faker.image.avatar()}
            /> 
        </ApprovalCard>

        <ApprovalCard>
            <CommentDetails 
              autor="Rossi"
              timeToGo="Today at 2:42PM" 
              content="that's Awesome. Thanks as well!"
              avatar={faker.image.avatar()}
            /> 
        </ApprovalCard> */
  };

  onSearchSubmit = async (term) => {
    console.log(term);

    const response = await unsplash.get("/search/photos", {
      params: { query: term },
    });

    //console.log(response.data.results);
    this.setState({ images: response.data.results });
  };



  onTermSubmit= async (term) => {
    console.log(term);
    const res = await youtube.get("/search", {
      params: {
        q: term,
      },
    });

    this.setState({ videos: res.data.items,
      onVideoSelect:res.data.items[0]});
      console.log(this.state.videos);

  }

  onVideoSelect = (video) => {
    console.log("App -getSelected Video", video);
    this.setState({ onVideoSelect: video });
  };


  onlanguagechange = (lang) =>{
    this.setState({language: lang});
  } 


  render() {
    return (
      <div>
        <div className="ui container comments">
          {/* {this.renderContent()} */}

          <ApprovalCard>
            <h4>Warning</h4>
            <p>Are you sure?</p>
          </ApprovalCard>
          {this.RenderListOfApproval()}

          <SearchBar SearchSubmit={this.onSearchSubmit} name="Sold me" />

          {/* {this.state.images} */}

          <ImageList images={this.state.images} />
        </div>

        <PostList />

        <div className="ui container grid">
          <div className="ui row">
            <div className="column eight wide">
              <SongList />
            </div>
            <div className="column eight wide">
              <SongDetails />
            </div>
          </div>
        </div>

        <div className="ui container">
          <SearchBar2 getDataSearchFieldValue={this.onTermSubmit} />
          <div className="ui grid">
            <div className="ui row">
              <div className="eleven wide column">
                <VideoDetails video={this.state.onVideoSelect} />
              </div>
              <div className="five wide column">
                <VideoList
        onVideoSelect={this.onVideoSelect}
        videos={this.state.videos}
      />
              </div>
            </div>
          </div>

          {/* I have found {this.state.videos.length} videos  */}
        </div>

        <div className="ui container">
                <div>
                Select a language
                <i className="flag us" onClick={()=> this.onlanguagechange('english')}/>
                <i className="flag nl" onClick={()=> this.onlanguagechange('dutch')}/>
                </div>
                <ColorContext.Provider value="negative">
                <LanguageContext.Provider  value={this.state.language}>
                    <UserCreate />
                </LanguageContext.Provider>
                </ColorContext.Provider>
            
                 {/* {this.state.language} */}

            </div>

            <div className="ui container">
            <Header />
            <BrowserRouter>
                <div>
                    <Route path="/" exact component={StreamList}/>
                    <Route path="/stream/create" exact component={StreamCreate}/>
                    <Route path="/stream/edit/:id" exact component={StreamEdit}/>
                    <Route path="/stream/delete/:id" exact component={StreamDelete}/>
                    <Route path="/stream/show" exact component={StreamShow}/>
                </div>
            </BrowserRouter>
        </div>

      </div>
    );
  }
}

export default App;
