import React from "react";
import { Component } from "react";
import LanguageContext from "../translate-context/context/LanguageContext";
import ColorContext from '../translate-context/context/ColorContext';
class Field extends Component{

    ///1
    //static contextType = LanguageContext;


    renderSubmit=(value)=>{
        return value === 'english' ? 'Submit' : 'Voorleggen';
    }
    
    render(){
        //2
        //const text = this.context === 'english' ? 'Submit' : 'Voorleggen';
        return(
            <div>
               <ColorContext.Consumer>
                   { color => 
                        <button className={`ui button ${color}`}>
                            <LanguageContext.Consumer>
                                {(value) => this.renderSubmit(value)}
                            </LanguageContext.Consumer> 
                        </button>
                   }
                </ColorContext.Consumer>   
                
            </div>
        );
    }
};

export default Field;