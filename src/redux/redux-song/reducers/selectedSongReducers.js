const selectedSongReducers = (selectedSong = null, action ) => {
    switch(action.type){
        case "SELECTED_SONG":
            return action.payload;
        default:
            return selectedSong;     
    }
        
}

export default selectedSongReducers;