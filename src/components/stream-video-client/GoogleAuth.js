import React, { Component } from "react";
import {connect} from 'react-redux';
import {signIn, signOut} from '../actions';

class GoogleAuth extends Component {
 // state = { isSignedIn: null };
  componentDidMount() {
    window.gapi.load("client:auth2", () => {
      window.gapi.client
        .init({
          clientId:
            "1041157250429-uk5gq8tijp6vr24m4pqtah87h5i50oiv.apps.googleusercontent.com",
          scope: "email",
        })
        .then(() => {
          this.auth = window.gapi.auth2.getAuthInstance();

          // this.onAuthChange();
          // this.auth.isSignedIn.listen(this.onAuthChange);


          this.onAuthChange(this.auth.isSignedIn.get());
          this.auth.isSignedIn.listen(this.onAuthChange);

        });
    });
  }

  // onAuthChange = () => {
  //   this.setState({ isSignedIn: this.auth.isSignedIn.get() });
  // };


  onAuthChange = (isSignedIn) => {
      if(isSignedIn){
          this.props.signIn(this.auth.currentUser.get().getId());
      }else{
        this.props.signOut();
      }
    };

  signOutClick = () => {
      this.auth.signOut();
  }
 
  signInClick = () => {
    this.auth.signIn();
  }

  RenderAuthButton() {
    if (this.props.isSignedIn === null) {
      return null;
    } else if (this.props.isSignedIn) {
      return (
        <div>
          <button onClick={() => this.signOutClick()} className="ui red google button">
            <i className="google icon" />
            Sign Out
          </button>
        </div>
      );
    } else {
      return (
        <div>
          <button onClick={() => this.signInClick()} className="ui red google button">
            <i className="google icon" />
            Sign In
          </button>
        </div>
      );
    }
  }

  render() {
    return <div>{this.RenderAuthButton()}</div>;
  }
}

const mapStateToProps = (state) => {
    console.log(state.auth);
    return {isSignedIn: state.auth.isSignedIn};
}

export default connect(mapStateToProps,{signIn,signOut})(GoogleAuth);
