

import React from 'react'

const CommentDetails = (props) => {
    return(
        <div className="comment">
            <a href="/" className="avatar">
                <img  alt="avatar" src={props.avatar} />
            </a>

            <div className="content">
                <a href="/" className="author">
                        {props.autor}
                </a>
                <div className="metadata">
                    <span className="date">
                        {props.timeToGo}</span>
                </div>

                <div className="text">
                    <p>
                        {props.content}
                    </p>
                </div>
            </div>
            
        </div>
    )
}

export default CommentDetails;