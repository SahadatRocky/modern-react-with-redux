import React from "react";
import { connect } from "react-redux";
const SongDetails = ({song}) => {
    if(!song){
        return(
            <div>
               select a song 
            </div>
        )
    }

    return(
        <div>
           <h3>Song Details</h3>
           <p>
                title: {song.title}
            <br />
                duration: {song.duration}
            </p> 
        </div>
    )
}

const mapStateToProps = (state) => {
    console.log(state.selectedSong);
    return {song: state.selectedSong}
}

export default connect(mapStateToProps)(SongDetails);