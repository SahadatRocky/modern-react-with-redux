
import { useEffect, useState } from "react";
import youtube from "../api/youtube"; 

const useVideos = (defaultSearchTerm) => {
    const [ videos, setVideos ] = useState([]);

    useEffect(()=> {
        search(defaultSearchTerm);
    }, [defaultSearchTerm]);

    const search = async (term) => {
        console.log(term);
        const res = await youtube.get("/search", {
          params: {
            q: term,
          },
        });
    
        setVideos(res.data.items);
        
      };

      return[ videos, search ];
};

export default useVideos;






///*****

/*
import React, { useEffect , useState} from "react";
// import youtube from "../api/youtube";
import SearchBar from "./SearchBar";
import VideoList from "./VideoList";
import VideoDetails from "./VideoDetails";
import useVideos from "../hooks/useVideos";

const  App = () =>  {
  const [videos, search ] = useVideos('buildings');
  const [onVideoSelect, setOnVideoSelect] = useState(null);
  useEffect(() => {
    setOnVideoSelect(videos[0]);
  }, [videos]);

  /*
  state = { videos: [], onVideoSelect: null };

  componentDidMount() {
      this.onTermSubmit("bmw");
  }

  onTermSubmit = async (term) => {
    console.log(term);
    const res = await youtube.get("/search", {
      params: {
        q: term,
      },
    });

    this.setState({ videos: res.data.items,
                    onVideoSelect:res.data.items[0]});
    console.log(this.state.videos);
  };
 
  onVideoSelect = (video) => {
    console.log("App -getSelected Video", video);
    this.setState({ onVideoSelect: video });
  };


    return (
      <div className="ui container">
        <SearchBar getDataSearchFieldValue={search} />
        <div className="ui grid">
          <div className="ui row">
            <div className="eleven wide column">
              <VideoDetails video={onVideoSelect} />
            </div>
            <div className="five wide column">
              <VideoList
                onVideoSelect={onVideoSelect}
                videos={videos}
              />
            </div>
          </div>
        </div>

        { I have found {this.state.videos.length} videos }
      </div>
    );
  }

export default App;

*/