import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import {fetchStreams} from '../../actions';

class StreamList extends Component {

    componentDidMount(){
       this.props.fetchStreams();
    }


    renderCreate = () => {
        if(this.props.isSignedIn){
            return(
                <div style={{textAlign: 'right'}}>
                    <Link to={'/stream/create'} className="ui button primary">
                       Create Stream
                    </Link>

                </div>
            );
        }
    }

    renderAdmin =(stream)=>{
        if(stream.userId === this.props.currentUserId){
         return(
             <div className="right floated content">
                 <Link to={`/stream/edit/${stream.id}`} className="ui button primary">
                    Edit
                 </Link>
                 <Link to={`/stream/delete/${stream.id}`} className="ui button negative">
                    Delete
                 </Link>
                 
             </div>
            );
        } 
    }

    renderList = () => {
       return this.props.streams.map(stream => {
            return(
                <div className="item" key={stream.id}>
                    <i className="large aligned icon camera"/>
                    <div className="content">
                        {stream.title} 
                        <div className="description">{stream.description}</div>
                    </div> 
                    <div>{this.renderAdmin(stream)}</div>
                </div>
            );
        });
    }

    render(){
       // console.log(this.props.streams);
        return(
            <div>
                <h2>Streams</h2>
                <div className="ui celled list">{this.renderList()}</div>
                {this.renderCreate()}
            </div>
        )
  }
}

const mapStateToProps = (state) => {
   // console.log(state.streams);
     return {
         streams: Object.values(state.streams),
         currentUserId : state.auth.userId,
         isSignedIn: state.auth.isSignedIn
        };
}

export default connect(mapStateToProps, {fetchStreams})(StreamList);