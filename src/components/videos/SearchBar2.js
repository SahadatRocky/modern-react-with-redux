import React from 'react';

class SearchBar2 extends React.Component{

    state = {term: ''};

    onFormSubmit = (e) => {
        e.preventDefault();
        console.log("submit");

        this.props.getDataSearchFieldValue(this.state.term);
        //ToDo Make sure we call
        // callback from parent component
    }

    render(){
        return(
            <div className="searchBar ui segment">
                <form onSubmit={this.onFormSubmit} className="ui form">
                    <div className="field">
                         <label>Video Search</label>
                         <input type="text" 
                         value={this.state.term}
                         onChange = {(e)=> {this.setState({ term: e.target.value }) }}
                         />
                    </div>
                </form>

            </div>
        );
    }
}

export default SearchBar2;