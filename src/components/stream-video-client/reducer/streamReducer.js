import {
    CREATE_STREAM,
    FETCH_STREAMS,
    FETCH_STREAM,
    EDIT_STREAM,
    DELETE_STREAM } from '../actions/types';


import _ from 'lodash';    
/*
//array base approch
const streamReducer = (state = [] , action) => {
    switch(action.type){
        case 'EDIT_STREAM' :
           return state.map(stream => {
              if(stream.id === action.payload.id){
                 return action.payload;
              }else{
                  return state;
              }
           });
        default:
            return state;

    }
}

*/

//object base approch
// eslint-disable-next-line import/no-anonymous-default-export
export default (state={}, action) => {
    switch(action.type){
        case FETCH_STREAMS:
            return {...state, ..._.mapKeys(action.payload, 'id')};   
        case EDIT_STREAM:
            // const newState = {...state, };
            // newState[action.payload.id] = action.payload;
            // return newState;
            return {...state, [action.payload.id] : action.payload};
        case FETCH_STREAM:
            return {...state, [action.payload.id] : action.payload};  
        case CREATE_STREAM:
            return {...state, [action.payload.id] : action.payload};   
        case DELETE_STREAM:
            return _.omit(state, action.payload);            
        default:
            return state;    
    }  
};