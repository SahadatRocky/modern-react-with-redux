
import React from 'react';
import VideoItem from './VideoItem';

const VideoList = ({videos,onVideoSelect})=> {
    const renderList = videos.map( (video) => {
        return <VideoItem key={video.id.videoId} onVideoSelect={onVideoSelect} video = {video} />
    });
   return(   
       <div className="ui relaxed divided list">
           {renderList}
        </div>
    //   <div>I have found {videos.length} videos</div>
   )
};

export default VideoList;